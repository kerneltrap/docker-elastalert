FROM alpine:3.12
LABEL MAINTAINER="Artyom Nosov <chip@unixstyle.ru>"

ARG VERSION=0.2.4

RUN set -x \
    && addgroup -S elastalert \
    && adduser -S -G elastalert elastalert

RUN set -x \
    && apk add --no-cache \
        ca-certificates \
        libffi \
        openssl \
        py3-pip \
        python3 \
        tini \
        su-exec \
        tzdata \
        wget \
    && apk add --no-cache --virtual .build-deps \
        gcc \
        libffi-dev \
        musl-dev \
        openssl-dev \
        python3-dev \
    && pip install elastalert==$VERSION \
    && apk --purge del .build-deps

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD [ "--config", "/config/config.yaml", "--verbose" ]
