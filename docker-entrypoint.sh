#!/bin/sh

set -e

# if flags are passed then prepend elastalert
if [ "${1:0:1}" = '-' ]; then
	set -- elastalert "$@"
fi

if [ "$1" = 'elastalert' ]; then
	set -- tini -- "$@"
	if [ "$(id -u)" = '0' ]; then
		set -- su-exec elastalert:elastalert "$@"
	fi
fi

exec "$@"
