Before you start you need to build an image

```bash
docker-compose build
```

To initialize indices you may use

```bash
docker-compose \
  -f docker-compose.yaml \
  -f docker-compose.extras.yaml \
    run --rm elastalert-create-index
```
